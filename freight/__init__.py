"""Freight is an Alliance Auth app for running a freight service."""

# pylint: disable = invalid-name
default_app_config = "freight.apps.FreightConfig"

__version__ = "1.12.0"
__title__ = "Freight"
